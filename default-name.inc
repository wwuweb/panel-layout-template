<?php

// Plugin definition
$plugin = array(
  'title' => t('Default Name'),
  'category' => t('Western'),
  'icon' => 'default-name.png',
  'theme' => 'default-name',
  'css' => '../../css/panel-layouts/default-name.css',
  'regions' => array(
    'top' => t('Top'),
    'middle_left' => t('Middle Left'),
    'middle_right' => t('Middle Right'),
    'bottom_left' => t('Bottom Left'),
    'bottom_center' => t('Bottom Center'),
    'bottom_right' => t('Bottom Right')
  ),
);
