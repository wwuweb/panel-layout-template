<?php
/**
 * @file
 * Template for the default-name page panel layout.
 *
 * This template provides a top row containing a single region, a middle row 
 * split into two equal regions and a
 * bottom row split into three equal regions.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout.
 */
?>
<div class="container-default-name clearfix">
    <div class="top-row-default-name clearfix">
    <div class="top-default-name clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['top']; ?>
    </div>
  
  </div>
    <div class="middle-row-default-name clearfix">
    <div class="middle-left-default-name clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['middle_left']; ?>
    </div>
    <div class="middle-right-default-name clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['middle_right']; ?>
    </div>
  </div>
  <div class="bottom-row-default-name clearfix">
    <div class="bottom-left-default-name clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['bottom_left']; ?>
    </div>
    <div class="bottom-center-default-name clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['bottom_center']; ?>
    </div>
    <div class="bottom-right-default-name clearfix" <?php if (!empty($css_id)) {print "id=\"$css_id\""; } ?>>
      <?php print $content['bottom_right']; ?>
    </div>
  </div>
</div>
