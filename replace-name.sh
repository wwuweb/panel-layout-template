#!/bin/bash

#===============================================================================
#
# FILE: replace-name.sh
#
# USAGE: ./replace-name.sh
# 
# DESCRIPTION: Create a new panel layout. Run this script to replace all instances of 
#   the previous (or default) panel layout's name with the new name for the layout.
#   The script will prompt for the target and replacement text,
#   allowing for flexibility and the ability to correct errors if the wrong
#   replacmeent text is entered. The script will also rename the .inc, .png, and 
#   .tpl.php files appropriately.
#   
#   The script will NOT rename the human-readable layout name in the .inc file.
#   This must be done manually.
#
#   grep, find, and sed are used to implement the functionality in this script.
#   grep shows the changes that will be made and allows the user to verify the
#   results. find locates the files in which the replacement will be performed,
#   and sed is responsible for the replacement operation.
#
# KNOWN ISSUES: This script fails to run on Mac OS X. It is recommended to run
#   from the WebTech Vagrant box or a similar Linux environment.
#
# ARGUMENTS:
#
# AUTHOR:
#   Nigel Packer 2015
# 
# MODIFIED:
#   Marshall Powell 2015
#===============================================================================

# Set error options for bash
set -o errexit
set -o nounset
set -o pipefail

# Real directory of the script, not current working directory
readonly SCRIPT_DIR=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

# Color escape sequences for coloring shell output
readonly GREEN='\033[0;32m'
readonly NC='\033[0m'

# Exclude search from directory
readonly EXCLUDE='.git'

# Print a message when exiting, possibly performing cleanup
__exitHandler() {
  wait

  local status=$?

  echo "Exiting"
  exit $status
}

# Trap errors and exit signals
trap __exitHandler \
  ERR EXIT SIGINT SIGTERM SIGQUIT

__main() {
  # Target text that will be searched and replaced
  local target=

  # Text with which target will be replaced
  local replacement=

  # Explain what the script will do
  read -p "  replace-name.sh replaces all instances of the current layout
  name with a new layout name. The script will NOT set the human-readable
  layout name in the .inc file. This must be done manually. Press Enter
  to continue or CTRL+C to quit."

  # Get the target text from the user
  read -p "Enter the layout name to replace, default-name by default: " target

  # Get the replacmeent text from the user
  read -p "Provide a new layout name, such as new-name: " replacement


  # Output operations that will be performed
  echo
  echo -e "${GREEN}Will rename '${target}.inc' to '${replacement}.inc'${NC}"
  echo -e "${GREEN}Will rename '${target}.png' to '${replacement}.png'${NC}"
  echo -e "${GREEN}Will rename '${target}.tpl.php' to '${replacement}.tpl.php'${NC}"
  echo
  echo -e "${GREEN}Will replace '${target}' in the following locations:${NC}"
  grep --exclude="build.sh" --exclude-dir="${EXCLUDE}" --recursive --color=auto "${target}" .
  echo

  local proceed=
  # Prompt the user to continue the replacement
  read -p "Continue with replacement? [Y/n]: " proceed

  if [ ! $proceed == "Y" ]; then
    exit 2
  fi

  # Replacmenet expression for sed command
  local expression='s/'"${target}"'/'"${replacement}"'/g'

  # Find all php files recusively in the directory and run sed to replace the
  # given patterns
  find \
    . \
    ! -name "replace-name.sh" \
    ! -name "${EXCLUDE}" \
    -type f \
    -exec sed \
    --in-place \
    --expression="${expression}" {} +

  # Rename the .inc, .png, and .tpl.php files
  mv "${target}.tpl.php" "${replacement}.tpl.php"
  mv "${target}.inc" "${replacement}.inc"
  mv "${target}.png" "${replacement}.png"

  # Output results of the operation
  echo
  echo -e "${GREEN}Renamed '${target}.inc' to '${replacement}.inc'${NC}"
  echo -e "${GREEN}Renamed '${target}.png' to '${replacement}.png'${NC}"
  echo -e "${GREEN}Renamed '${target}.tpl.php' to '${replacement}.tpl.php'${NC}"
  echo
  echo -e "${GREEN}Replaced '${target}' with '${replacement}':${NC}"
  grep --exclude="replace-name.sh" --exclude-dir="${EXCLUDE}" --recursive --color=auto "${replacement}" .
  echo

  exit 0
}

__main $@
